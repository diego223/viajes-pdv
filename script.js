 
function fadeOut(event) {
    event.preventDefault();
    var target = event.target.getAttribute("href");
    var content = document.getElementById("content");
    if (target != "#inicio") {
        setTimeout(function () {
            window.location.href = target;
        }, 1000);
        content.classList.remove("show");
    }
}

document.addEventListener("DOMContentLoaded", function () {
    var content = document.getElementById("content");
    content.classList.toggle("show");

});

function scrollToTop() {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }

  window.onscroll = function() {
    scrollFunction();
  };
  
  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById("scrollBtn").style.display = "block";
    } else {
      document.getElementById("scrollBtn").style.display = "none";
    }
  }
  