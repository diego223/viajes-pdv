var translations;

function loadTranslations(callback) {
  var xhr = new XMLHttpRequest();
  xhr.overrideMimeType("application/json");
  xhr.open('GET', 'translations.json', true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      translations = JSON.parse(xhr.responseText);
      callback();
    }
  };
  xhr.send(null);
}

document.addEventListener('DOMContentLoaded', function() {
  loadTranslations(function () {
    changeLanguage();
  });
});

function changeLanguage() {
  var currentLanguage = document.documentElement.lang;
  var languageButton = document.getElementById('language-button');
  var homeElement = document.getElementsByName('navbar.home')[0];
  var reserveElement = document.getElementsByName('navbar.reserve')[0];
  //titulo 1
  var titleElement = document.getElementsByName('index.page.main.title')[0];
  var indexPageMainP1 = document.getElementsByName('index.page.main.p1')[0];
  var indexPageMainP2 = document.getElementsByName('index.page.main.p2')[0];
  var indexPageMainP3 = document.getElementsByName('index.page.main.p3')[0];
  var indexPageMainP4 = document.getElementsByName('index.page.main.p4')[0];
  var indexPageMainP5 = document.getElementsByName('index.page.main.p5')[0];
  var indexPageMainP6 = document.getElementsByName('index.page.main.p6')[0];
  //cards del index
  var indexCardTitle1 = document.getElementsByName('index.card.title.1')[0];
  var indexCardTitle2 = document.getElementsByName('index.card.title.2')[0]; 
  var indexCardTitle3 = document.getElementsByName('index.card.title.3')[0];
  var indexCardP1 = document.getElementsByName('index.card.p.1')[0];
  var indexCardP2 = document.getElementsByName('index.card.p.2')[0];
  var indexCardP3 = document.getElementsByName('index.card.p.3')[0];
  //footer
  var footer = document.getElementsByName('footer')[0];  
   
  
  if (currentLanguage === 'es') {
    document.documentElement.lang = 'en';
    languageButton.innerHTML = translations.en['language-button'];
    homeElement.innerHTML = translations.en['navbar.home'];
    reserveElement.innerHTML = translations.en['navbar.reserve'];
    //titulo 1 en
    titleElement.innerHTML = translations.en['index.page.main.title'];
    indexPageMainP1.innerHTML = translations.en['index.page.main.p1'];
    indexPageMainP2.innerHTML = translations.en['index.page.main.p2'];
    indexPageMainP3.innerHTML = translations.en['index.page.main.p3'];
    indexPageMainP4.innerHTML = translations.en['index.page.main.p4'];
    indexPageMainP5.innerHTML = translations.en['index.page.main.p5'];
    indexPageMainP6.innerHTML = translations.en['index.page.main.p6'];
    //cards del index en
    indexCardTitle1.innerHTML = translations.en['index.card.title.1'];
    indexCardTitle2.innerHTML = translations.en['index.card.title.2']; 
    indexCardTitle3.innerHTML = translations.en['index.card.title.3'];
    indexCardP1.innerHTML = translations.en['index.card.p.1'];
    indexCardP2.innerHTML = translations.en['index.card.p.2'];  
    indexCardP3.innerHTML = translations.en['index.card.p.3'];
    //footer
    footer.innerHTML = translations.en['footer'];    




  } else {
    document.documentElement.lang = 'es';
    languageButton.innerHTML = translations.es['language-button'];
    homeElement.innerHTML = translations.es['navbar.home'];
    reserveElement.innerHTML = translations.es['navbar.reserve'];
    
    //titulo 1 es
    titleElement.innerHTML = translations.es['index.page.main.title'];
    indexPageMainP1.innerHTML = translations.es['index.page.main.p1'];
    indexPageMainP2.innerHTML = translations.es['index.page.main.p2'];
    indexPageMainP3.innerHTML = translations.es['index.page.main.p3'];
    indexPageMainP4.innerHTML = translations.es['index.page.main.p4'];
    indexPageMainP5.innerHTML = translations.es['index.page.main.p5'];
    indexPageMainP6.innerHTML = translations.es['index.page.main.p6'];
    //cards del index es
    indexCardTitle1.innerHTML = translations.es['index.card.title.1'];
    indexCardTitle2.innerHTML = translations.es['index.card.title.2']; 
    indexCardTitle3.innerHTML = translations.es['index.card.title.3'];
    indexCardP1.innerHTML = translations.es['index.card.p.1'];
    indexCardP2.innerHTML = translations.es['index.card.p.2'];  
    indexCardP3.innerHTML = translations.es['index.card.p.3'];
    //footer
    footer.innerHTML = translations.es['footer'];         




  }
}
